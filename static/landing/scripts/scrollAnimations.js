window.sr = ScrollReveal({ reset: true });
sr.reveal('.header__image', { origin: 'bottom', duration: 500, scale: 1, distance: '100px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)'});
sr.reveal('.features__image', { origin: 'bottom', duration: 500, scale: 1, distance: '100px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)'});
sr.reveal('.features__description', {origin: 'right', distance: '20px', duration: 500});
sr.reveal('.comments__image', { origin: 'bottom', duration: 500, scale: 1, distance: '100px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)'});
sr.reveal('.comments__description', {origin: 'right', distance: '20px', duration: 500});
