$pen = $("#button-content__pen");
$lib = $('#button-content__lib');
$helpingtools = $(".tool-button");
$pentools = $("#empty-space__additional-tools");
$libtools = $('#empty-space__additional-tools_lib');

$helpingtools.click(function(){
    if($pen.parents(".tool-button__selected").length){
        $libtools.fadeOut(500, function () {
          $pentools.fadeIn(500);
        });

    } else if($lib.parents(".tool-button__selected").length){
        $pentools.fadeOut(500, function () {
          $libtools.fadeIn(500);
        });
    } else {
        console.log("This... wasn't supposed to happen. There is an error in the $helpingtools.click function.");
    }
});

$toolcontainer =  $("#button-draw");
$toolcontainer2 = $("#button-erase");
$toolcontainer3 = $("#button-move");
$toolcontainers = [$toolcontainer, $toolcontainer2, $toolcontainer3];
$colorsel = $("#color-selector");
$widthsel = $('#width-selector');
var mode = 0;
var nextmode = 0;

$toolcontainer.click(function(){
    if($toolcontainer.parent().hasClass("pen-tools__item__unselected")){
        nextmode = 0;
        toggleTools();
        $toolcontainer2.css("fill","#cccccc");
        $toolcontainer3.css("fill","#cccccc");
        $toolcontainer.css("fill",$(".color-selector__selected svg circle").attr("fill"));
        $colorsel.css("visibility", "visible");
        $colorsel.animate({opacity: 1}, 300);
        $widthsel.css("visibility", "visible");
        $widthsel.animate({opacity: 1}, 300);
    }
});

$toolcontainer2.click(function(){
    if($toolcontainer2.parent().hasClass("pen-tools__item__unselected")){
        nextmode = 1;
        toggleTools();
        $toolcontainer.css("fill","#cccccc");
        $toolcontainer3.css("fill","#cccccc");
        $toolcontainer2.css("fill","#333333");
        $colorsel.animate({opacity: 0}, 300, function() {
            $colorsel.css("visibility", "hidden");
        });
        $widthsel.animate({opacity: 0}, 300, function() {
            $widthsel.css("visibility", "hidden");
        });

    }
});

$toolcontainer3.click(function(){
    if($toolcontainer3.parent().hasClass("pen-tools__item__unselected")){
      nextmode = 2;
      toggleTools();
      $toolcontainer.css("fill","#cccccc");
      $toolcontainer2.css("fill","#cccccc");
      $toolcontainer3.css("fill","#333333");
      $colorsel.animate({opacity: 0}, 300, function() {
          $colorsel.css("visibility", "hidden");
      });
      $widthsel.animate({opacity: 0}, 300, function() {
          $widthsel.css("visibility", "hidden");
      });
    }
});



$colorseldivs = $("#color-selector div");

$colorseldivs.click(function(){
    if($toolcontainer.parent().hasClass("pen-tools__item__selected")) {
        //toggles selected colour
        $(".color-selector__selected").toggleClass("color-selector__selected");
        $(this).toggleClass("color-selector__selected");

        // Changes the background color of the pen tool according to the currently selected colour.
        $toolcontainer.css("fill", $(".color-selector__selected svg circle").attr("fill"));
    }
});

function toggleTools(){
  $toolcontainers[mode].parent().toggleClass("pen-tools__item__selected pen-tools__item__unselected");
  $toolcontainers[nextmode].parent().toggleClass("pen-tools__item__selected pen-tools__item__unselected");

  mode = nextmode;
}
