var $el, $ps, $up, totalHeight;
$description = $("#stream-desc__text");
$readmore = $("#stream-desc__text #read-more-btn");
if ($description.outerHeight() >= 120){
    $description.addClass('is_overflow');
    $readmore.css("visibility", "visible");
}

$("#stream-desc__text .button").click(function() {

    totalHeight = 0;

    $el = $(this);
    $p = $el.parent();
    $up = $p.parent();
    $ps = $up.find("p:not('#read-more-btn')");

    $ps.each(function () {
        totalHeight += $(this).outerHeight();
    });

    $up
        .css({
            "height": $up.height(),
        })
        .animate({
            "height": totalHeight
        });

    $("#stream-info__details").css("max-height", "inherit");
    $p.fadeOut();

    return false;
});