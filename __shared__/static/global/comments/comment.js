$(function () {
  var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
  var ws_path = ws_scheme + "://" + window.location.host + "/comments/stream/";
  console.log("Connecting to " + ws_path);
  var socket = new ReconnectingWebSocket(ws_path);
  var client_cursor = 0;

  socket.onopen = function () {
    console.log("Connected to comment socket");
  };

  socket.onclose = function () {
    $(".send-comment").off("click");
    console.log("Disconnected from comment socket");
  }

  socket.onmessage = function (message) {
    console.log("(Comment) Got websocket message " + message.data);
    var data = JSON.parse(message.data);
    if (data.connect) {
      roomId = $(".room").attr("data-room-id");
      socket.send(JSON.stringify({
        "command": "join",
        "room": roomId,
        "client_cursor": client_cursor,
      }));
    } else if (data.join) {
      // set button to send comment when clicked
      $(".send-comment").on("click", function () {
        let message = $(".comment-box").val();
        if (!message.replace(/\s/g, '').length) {
          return;
        }

        socket.send(JSON.stringify({
          "command": "send",
          "room": data.join,
          "message": $(".comment-box").val()
        }));
        $(".comment-box").val("");
      })

      // set input text to send comment when enter key is pressed
      $(".comment-box").keypress(function (event) {
        if (event.which == 13) {
          let message = $(".comment-box").val();
          if (!message.replace(/\s/g, '').length) {
            return;
          }

          socket.send(JSON.stringify({
            "command": "send",
            "room": data.join,
            "message": $(".comment-box").val(),
          }));
          $(".comment-box").val("");
        }
      });

      previous_comments = data.previous_comments;
      if (previous_comments) {
        for(comment in previous_comments) {
          render_message(previous_comments[comment]);
        }
      }

      // enable send comment button only when previous message have been loaded
      $('.send-comment').prop('disabled', false);
    } else if (data.leave) {
        // behavior specific to the user that left goes here
    } else {
      render_message(data)
    }
  }

  const render_message = (data) => {
    switch(data.msg_type) {
      case 0:
        commentDiv = `<div class='comment-block'>
                        <span class='commenter-name'> ${data.username} </span>
                        <span class='comment-text'> ${data.message} </span>
                      </div>`;
        $(".comments-board").append(commentDiv);
        client_cursor++;
        break;
      case 1:
        var user = $(".users").find(`[data-user=${data.username}]`);
        if(!user.attr("data-user")) {
          var userdiv = $(
            "<div class='user' data-user='" + data.username + "'>" +
            data.username +
            "</div>"
          );
          $(".users").append(userdiv);
        }
        break;
      case 2:
        userDiv = $('.user[data-user="' + data.username + '"]');
        userDiv.remove();
        break;
      default:
        return;
    }
  };
})
