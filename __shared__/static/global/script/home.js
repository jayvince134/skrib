
 // Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("streambtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// Get the modal
var profmodal = document.getElementById("prof-modal");

// Get the button that opens the modal
var profbtn = document.getElementById("profbtn");

// When the user clicks on the button, open the modal 
btn.onclick = function(event) {
    event.preventDefault();
    modal.style.display = "flex";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

profbtn.onclick = function(event) {
    event.preventDefault();
    profmodal.style.display = "flex";
}

window.addEventListener("click", function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    if(event.target == profmodal) {
        profmodal.style.display = "none";
    }
    
});

$(document).ready(function(){
    $("#show-more").click(function(){
        $(".hidden-trending").slideToggle("fast", function(){
        	$("#show-more").hide();
        });
        
    });
});

$(document).ready(function(){
    $("#show-jff").click(function(){
        $(".hidden-jff").slideToggle("fast", function(){
        	$("#show-jff").hide();
        });
        
    });
});	

(function() {
  $(document).ready(function() {
    var invite = document.getElementById("invite-people");
    $('.switch-input').on('change', function() {
      var isChecked = $(this).is(':checked');
      var selectedData;
      var $switchLabel = $('.switch-label');
      console.log('isChecked: ' + isChecked); 

      if(isChecked) {
        selectedData = $switchLabel.attr('data-on');
      } else {
        selectedData = $switchLabel.attr('data-off');
      }
      console.log('Selected data: ' + selectedData);

    });
  });

})();