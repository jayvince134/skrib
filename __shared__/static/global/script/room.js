var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var svg = document.getElementById('sample'); // the entire SVG canvas
var dragging = false; // a flag to indicate whether the user is drawing
var x_offset = 230; // the offset to the x axis
var motion = ""; // the path drawn by the user
var color = "black" , size = 3; // the color and size of the pen
var selectedElement = false; // refers to the scribble object being moved
var userInteraction = []; // the array of what the user does, used in undo and redo
var viewMode = "scribble"; // the mode of the user interaction
var radius = 0.5; // retain this for now as it is used by websocket
let pdf_canvas = document.getElementById('pdf-canvas');
let cursor_canvas = document.getElementById('canvas-cursor');
let cursorPos;

var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
var ws_path = ws_scheme + "://" + window.location.host + "/scribble/stream/";
console.log("Connecting to " + ws_path);
var socket = new ReconnectingWebSocket(ws_path);
var client_cursor = 0;
var scribble_id = 0;
var message_queue = [];
var interval_message_sending;
let presentor_id = document.getElementsByClassName("presentor-id")[0].value;
let user_id = document.getElementsByClassName("user-id")[0].value;
let rendered_view_mode;

$("#svgHolder").ready(function() {
  $("#btnScribble").trigger('click');
  $("#Canvas").trigger('click');
  console.log("READYYY");
});

socket.onopen = function () {
  console.log("Connected to room socket");
  $("#btnScribble").trigger('click');
  $("#Canvas").trigger('click');
};

socket.onclose = function () {
  console.log("Disconnected from room socket");
}

socket.onmessage = function (message) {
  // console.log("(Scribble) Got websocket message " + message.data);
  var data = JSON.parse(message.data);
  if (data.connect) {
    roomId = document.getElementsByClassName("roomId")[0].value;
    socket.send(JSON.stringify({
      "command": "join",
      "room": roomId,
      "client_cursor": client_cursor,
    }));
  } else if (data.join) {

    // render previous messages saved
    previous_scribbles = data.previous_scribbles;
    if (previous_scribbles) {
      for(scribble in previous_scribbles) {
        parsed_message = JSON.parse(previous_scribbles[scribble]);
        for (let i = 0; i < parsed_message.message.length; i++) {
            render_message(parsed_message.room, parsed_message.username, parsed_message.message[i], true);
        }
        client_cursor++
      }
    }

    // initalize listeners only after previous messages have been rendered
    setTimeout(initListeners);

    // set to scribble mode
    setTimeout(scribbleElement);

    // show canvas only after loading previous messages to hide the rendering of other scribbles
    setTimeout(function () {
      $(svg).show();
    });

    // load PDF file
    if (data.pdf_file) {
      $("#upload-button").hide();
      $("#pdf-navi").show();
      $("#canvasNavi").show();
      $("button[id^=PDF]").removeAttr("disabled");
      $("#PDFCanvas").trigger('click');

      let blobAsFile = dataURItoBlob(data.pdf_file, null);
      let blobFile = blobToFile(blobAsFile, data.pdf_file_name);
      showPDF(URL.createObjectURL(blobFile), true, true);
    }

    // set view mode
    setTimeout(function () {
      if (rendered_view_mode) {
        $("#canvasNavi > button").removeAttr("style");
        $(`#${rendered_view_mode}`).attr("style", "background-color: #2595f7;");
      }
    });
  } else if (data.message.event == 'send_cursor') {
    let username = document.getElementsByClassName("username")[0].value;
    if (data.username != username) {
      let context = cursor_canvas.getContext('2d');
      context.clearRect(0, 0, cursor_canvas.width, cursor_canvas.height);
      let radius = 4;

      context.beginPath();
      context.arc(data.message.x, data.message.y + 10, radius, 0, 2 * Math.PI, false);
      context.fillStyle = 'blue';
      context.fill();
      context.lineWidth = 2;
      context.strokeStyle = '#1919FF';
      context.stroke();
    }
  } else {
    let message = data.message;
    for (let i = 0; i < message.length; i++) {
        render_message(data.room, data.username, message[i], false);
    }
    client_cursor++
  }
}

const sendMessage = (message) => {
  message_queue.push(message);
  if (!interval_message_sending) {
    interval_message_sending = setInterval(function () {
      if (message_queue.length == 0) {
        clearInterval(interval_message_sending);
        interval_message_sending = null;
      } else {
        socket.send(JSON.stringify({
          "command": "send",
          "room": document.getElementsByClassName("roomId")[0].value,
          "message": message_queue,
        }));
        message_queue.length = 0;
      }
    }, 400);
  }
}

const keyPress = (e) => {
  let evtobj = window.event? event : e;
  if (evtobj.keyCode == 90 && evtobj.ctrlKey) {
    if (!btnUndo.disabled) {
      undo();
      document.onkeydown = null;
      setTimeout(function () {
        document.onkeydown = keyPress;
      }, 200);
    }
  } else if (evtobj.keyCode == 89 && evtobj.ctrlKey) {
    if (!btnRedo.disabled) {
      redo();
      document.onkeydown = null;
      setTimeout(function () {
        document.onkeydown = keyPress;
      }, 200);
    }
  }
}

const initListeners = () => {
  if (presentor_id == user_id) {
    btnRedo = document.getElementById('btnRedo');
    btnUndo = document.getElementById('btnUndo');
    btnClear = document.getElementById('btnClear');
    btnMove = document.getElementById('btnMove');
    btnScribble = document.getElementById('btnScribble');
    btnErase = document.getElementById('btnErase');

    document.getElementById('sample').addEventListener('mousedown', engage);
    document.getElementById('sample').addEventListener('mousemove', putPoint);
    document.getElementById('sample').addEventListener('mouseup', disengage);
    document.getElementById('sample').addEventListener('mouseleave', disengage);
    document.getElementById('sample').addEventListener('mousemove', cursorMove);
    document.getElementById('sample').addEventListener('mousedown', startDrag);
    document.getElementById('sample').addEventListener('mousemove', drag);
    document.getElementById('sample').addEventListener('mouseup', endDrag)

    btnRedo.addEventListener('click', redo);
    btnUndo.addEventListener('click', undo);
    btnClear.addEventListener('click', clearAll);
    btnMove.addEventListener('click', moveElement);
    btnScribble.addEventListener('click', scribbleElement);
    btnErase.addEventListener('click', eraseElement);
    document.onkeydown = keyPress;

    $("#pdf").prop('disabled', false);

    $("#pdf").on('change', function() {
      if ($("#pdf").get(0).files[0]) {
        $("#objPdf").attr("data", URL.createObjectURL($("#pdf").get(0).files[0]));
      }
    });

    // checkbox events
    $("#canvasNavi > button").on('click', function() {
      if (this.id == "PDF") {
        $("#svgHolder").attr("style", "width:100%; height:100%; position:absolute; display: none;");
        $("#pdf-container").attr("style", "text-align:center; width:100%; height:100%; position:absolute; pointer-events:none;");
        // $("#objects").attr("style", "display: none;");
        sendMessage({
          "event": "checkboxPDF",
        });
        // enable prev and next buttons
        $("#pdf-next").removeAttr("disabled");
        $("#pdf-prev").removeAttr("disabled");
      } else if (this.id == "PDFCanvas") {
        $("#svgHolder").attr("style", "width:100%; height:100%; position:absolute;");
        $("#pdf-container").attr("style", "text-align:center; width:100%; height:100%; position:absolute; pointer-events:none;");
        // $("#objects").removeAttr("style");

        $("svg[id=" + __CURRENT_PAGE + "]").show();
        $("#svgHolder > svg[id!=" + __CURRENT_PAGE + "]").hide();
        svg = $("svg[id=" + __CURRENT_PAGE + "]")[0];
        sendMessage({
          "event": "checkboxPDFCanvas",
        });
        // enable prev and next buttons
        $("#pdf-next").removeAttr("disabled");
        $("#pdf-prev").removeAttr("disabled");
      } else {
        $("#sample").attr("style", "width:100%; height:100%; position:absolute;");
        $("#pdf-container").attr("style", "text-align:center; width:100%; height:100%; position:absolute; pointer-events:none; display:none;");
        // $("#objects").removeAttr("style");

        $("#svgHolder > svg[id!=sample]").hide();
        $("#sample").show();
        svg = $("#sample")[0];
        sendMessage({
          "event": "checkboxCanvas",
        });
        // disable prev and next buttons
        $("#pdf-next").attr("disabled", "disabled");
        $("#pdf-prev").attr("disabled", "disabled");
      }
      $("#canvasNavi > button").removeAttr("style");
      $(this).attr("style", "background-color: #2595f7;");
    });

    // Upon click this should should trigger click on the #file-to-upload file input element
    // This is better than showing the not-good-looking file input element
    $("#upload-button").on('click', function() {
      $("#pdf").trigger('click');
    });

    // When user chooses a PDF file
    $("#pdf").on('change', function() {
      // Check whether a file is chosen
      if (!$("#pdf").get(0).files[0]) {
        return;
      }

      // Validate whether PDF
        if(['application/pdf'].indexOf($("#pdf").get(0).files[0].type) == -1) {
            alert('Error : Not a PDF');
            return;
        }

      $("#upload-button").hide();
      $("#pdf-navi").show();
      $("#canvasNavi").show();
      $("button[id^=PDF]").removeAttr("disabled");
      $("#PDFCanvas").trigger('click');

      // reset PDF SVGs
      $("#svgHolder > svg[id!=sample]").remove();
      // reset user interactions
      userInteractionNew = [];
      for (let i = 0; i < userInteraction.length; i++) {
        if (userInteraction[i].page == 'sample') {
          userInteractionNew.push(userInteraction[i]);
        }
      }
      userInteraction = userInteractionNew;

      // Send the object url of the pdf
      showPDF(URL.createObjectURL($("#pdf").get(0).files[0]));

      // Send pdf file data to other users
      let file = $("#pdf").get(0).files[0];
      let fileAsBlob = new Blob([file]);

      let reader = new FileReader();
      reader.onloadend = function() {
      base64data = reader.result;

        sendMessage({
          "event": "openPDF",
          "file": base64data,
          "filename": file.name,
        });
      }
      reader.readAsDataURL(fileAsBlob);
    });

    // Previous page of the PDF
    $("#pdf-prev").on('click', function() {
        console.log("Current Page is " + __CURRENT_PAGE + " before");
      if(__CURRENT_PAGE != 1) {
        console.log("Current Page is " + __CURRENT_PAGE);
        showPage(--__CURRENT_PAGE);

        sendMessage({
          "event": "prev_page_PDF",
        });
      }
    });

    // Next page of the PDF
    $("#pdf-next").on('click', function() {
        console.log("Current Page is " + __CURRENT_PAGE + " which is not equal to " + __TOTAL_PAGES + " before");
      if(__CURRENT_PAGE != __TOTAL_PAGES) {
        console.log("Current Page is " + __CURRENT_PAGE + " which is not equal to " + __TOTAL_PAGES);
        showPage(++__CURRENT_PAGE);
        console.log("Current Page is " + __CURRENT_PAGE + " which is not equal to " + __TOTAL_PAGES + " after");

        sendMessage({
          "event": "next_page_PDF",
        });
        if (__CURRENT_PAGE == __TOTAL_PAGES) {
          $("#pdf-next").attr("disabled", "true");
        }
      }
    });
  } else {
    $("#upload-button").attr("disabled", "true");
    $("#pdf-prev").attr("disabled", "true");
    $("#pdf-next").attr("disabled", "true");
    $("#canvasNavi > button").attr("disabled", "true");
  }
};

const getCurrentPage = () => {
  if (!$("#sample").is(":hidden")) {
    return "sample";
  } else {
    return __CURRENT_PAGE;
  }
};

const getPageUserInteraction = () => {
  for (let i = 0; i < userInteraction.length; i++) {
    if (userInteraction[i].page == getCurrentPage()) {
      return userInteraction[i];
    }
  }
  return null;
};

const updateUserInteraction = (pageUserInteraction) => {
  console.log("update user interaction: " + pageUserInteraction.head);
  for (let i = 0; i < userInteraction.length; i++) {
    if (userInteraction[i].page == pageUserInteraction.page) {
      userInteraction[i] = pageUserInteraction;
      return;
    }
  }
  userInteraction.push(pageUserInteraction);
};

// helper function to convert base64 to blob
function dataURItoBlob(dataURI, callback) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  return new Blob([ab], {type: mimeString});
}

// helper function to convert blob to file
function blobToFile(theBlob, fileName){
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return theBlob;
}

// helper function to get scribble based on scribble_id and scribble_user
const getScribble = (scribble_id, scribble_user) => {
  let group = svg.children;
  for (let i = 0; i < group.length; i++) {
    if (group[i].getAttribute("scribble_id") == scribble_id && group[i].getAttribute("scribble_user") == scribble_user) {
      return group[i];
    }
  }
};

// helper function to create scribble based on given properties
const createScribble = (scribbleProperties) => {
  // rendering the line in SVG
  let newG = document.createElementNS('http://www.w3.org/2000/svg', "g");
  newG.setAttribute("class", scribbleProperties.class);
  newG.setAttribute("visibility", scribbleProperties.visibility);
  newG.setAttribute("scribble_id", scribbleProperties.scribble_id);
  newG.setAttribute("scribble_user", scribbleProperties.scribble_user);
  // this will create a transparent object to help facilitate dragging
  let newpath = document.createElementNS('http://www.w3.org/2000/svg', "path");
  newpath.setAttribute("d", scribbleProperties.path1.d);
  newpath.setAttribute("fill", scribbleProperties.path1.fill);
  newpath.setAttribute("visibility", scribbleProperties.path1.visibility);
  newpath.setAttribute("stroke", scribbleProperties.path1.stroke);
  newpath.setAttribute("stroke-width", scribbleProperties.path1.stroke_width);
  newpath.setAttribute("opacity", scribbleProperties.path1.opacity);
  newG.appendChild(newpath);
  // this is the exact scribble made by user
  newpath = document.createElementNS('http://www.w3.org/2000/svg',"path");
  newpath.setAttribute("d", scribbleProperties.path2.d);
  newpath.setAttribute("fill", scribbleProperties.path2.fill);
  newpath.setAttribute("visibility", scribbleProperties.path2.visibility);
  newpath.setAttribute("stroke", scribbleProperties.path2.stroke);
  newpath.setAttribute("stroke-width", scribbleProperties.path2.stroke_width);
  newG.appendChild(newpath);
  svg.appendChild(newG);
};

const getScribbleProperties = (scribble) => {
  let scribbleProperties = {
    "class": scribble.getAttribute("class"),
    "visibility": scribble.getAttribute("visibility"),
    "scribble_id": scribble.getAttribute("scribble_id"),
    "scribble_user": scribble.getAttribute("scribble_user"),
    "path1": {
      "d": scribble.children[0].getAttribute("d"),
      "fill": scribble.children[0].getAttribute("fill"),
      "visibility": scribble.children[0].getAttribute("visibility"),
      "stroke": scribble.children[0].getAttribute("stroke"),
      "stroke_width": scribble.children[0].getAttribute("stroke-width"),
      "opacity": scribble.children[0].getAttribute("opacity"),
    },
    "path2": {
      "d": scribble.children[1].getAttribute("d"),
      "fill": scribble.children[1].getAttribute("fill"),
      "visibility": scribble.children[1].getAttribute("visibility"),
      "stroke": scribble.children[1].getAttribute("stroke"),
      "stroke_width": scribble.children[1].getAttribute("stroke-width"),
    },
  };
  return scribbleProperties;
};

const render_message = (message_room, message_username, message, render_from_reconnection) => {
  console.log("message: " + message.event);
  let username = document.getElementsByClassName("username")[0].value;
  if (message.event == 'disengage') {
    console.log("render_message disengage");
    if (render_from_reconnection || message_username != username) {
      setTimeout(render_disengage, 0, message_room, message_username, message);
    }
  } else if (message.event == 'engage') {
    if (message_username != username) {
      setTimeout(render_engage, 0, message);
    }
  } else if (message.event == 'putPoint') {
    if ((message_username != username) && !render_from_reconnection) {
      setTimeout(render_putpoint, 0, message);
    }
  } else if (message.event == 'undo') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received undo");
      setTimeout(render_undo, 0, message);
    }
  } else if (message.event == 'redo') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received redo");
      setTimeout(render_redo, 0, message);
    }
  } else if (message.event == 'start_drag') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received start_drag");
      setTimeout(render_start_drag, 0, message);
    }
  } else if (message.event == 'end_drag') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received end_drag");
      setTimeout(render_end_drag, 0, message);
    }
  } else if (message.event == 'erase') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received erase");
      setTimeout(render_erase, 0, message);
    }
  } else if (message.event == 'clear_all') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received clear all");
      setTimeout(render_clear_all, 0, message);
    }
  } else if (message.event == 'openPDF') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received openPDF");
      setTimeout(render_openPDF, 0, message, render_from_reconnection);
    }
  } else if (message.event == 'prev_page_PDF') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received prev_page_PDF");
      setTimeout(render_prev_page_PDF, 0, message, render_from_reconnection);
    }
  } else if (message.event == 'next_page_PDF') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received next_page_PDF");
      setTimeout(render_next_page_PDF, 0, message, render_from_reconnection);
    }
  } else if (message.event == 'checkboxPDF') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received checkboxPDF");
      setTimeout(render_checkboxPDF);
    }
  } else if (message.event == 'checkboxPDFCanvas') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received checkboxPDFCanvas");
      setTimeout(render_checkboxPDFCanvas)
    }
  } else if (message.event == 'checkboxCanvas') {
    if (render_from_reconnection || message_username != username) {
      console.log("@received checkboxCanvas");
      setTimeout(render_checkboxCanvas);
    }
  }
};

const render_disengage = (message_room, message_username, message) => {
  console.log("@receive disengage");
  context.beginPath();
  context.clearRect(0, 0, canvas.width, canvas.height); // delete canvas drawing

  // rendering the line in SVG
  createScribble(message.scribble);

  // update scribble_id if reconnected from disconnection
  let username = document.getElementsByClassName("username")[0].value;
  if (message_username == username) {
    scribble_id++;
  }
};

const render_engage = (message) => {
  console.log("@receive engage");
};

const render_putpoint = (message) => {
  console.log("@receive putpoint");
  console.log("putting point");
  context.lineTo(message.x, message.y);
  context.stroke();
  context.beginPath();
  context.arc(message.x, message.y, message.radius, 0, Math.PI*2);
  context.fill();
  context.beginPath();
  context.moveTo(message.x, message.y);
};

const render_undo = (message) => {
  console.log("@receive undo");
  let group = svg.children;
  let paths;
  let scribble_to_undo;

  switch (message.undo_event) {
    case "disengage":
      console.log("undo disengage");
      scribble_to_undo = getScribble(message.undo_scribble.scribble_id, message.undo_scribble.scribble_user);
      if (scribble_to_undo) {
          svg.removeChild(scribble_to_undo);
      }
      break;
    case "erase":
      console.log("undo erase");
      createScribble(message.undo_scribble);
      break;
    case "clear_all":
      console.log("undo clear");
      let scribbles = message.undo_scribbles;
      for (let i = 0; i < scribbles.length; i++) {
        createScribble(scribbles[i]);
      }
      break;
    default:
      return;
  }
};

const render_redo = (message) => {
  console.log("@receive redo");
  let group = svg.children;
  let paths;
  let scribble_to_redo;

  switch (message.redo_event) {
    case "disengage":
      console.log("redo disengage");
      createScribble(message.redo_scribble);
      break;
    case "erase":
      console.log("redo erase");
      scribble_to_redo = getScribble(message.redo_scribble.scribble_id, message.redo_scribble.scribble_user);
      if (scribble_to_redo) {
        svg.removeChild(scribble_to_redo);
      }
      break;
    case "clear_all":
      console.log("redo clear");
      while (svg.firstChild) {
        svg.removeChild(svg.firstChild);
      }
      break;
    default:
      return;
  }
};

const render_start_drag = (message) => {
  let paths = svg.children;
  selectedElement_receive = "";
  for (let i = 0; i < paths.length; i++) {
    if (paths[i].getAttribute("scribble_id") == message.scribble_id && paths[i].getAttribute("scribble_user") == message.scribble_user) {
      selectedElement_receive = paths[i];
      break;
    }
  }

  var transforms_receive = selectedElement_receive.transform.baseVal;
  if (transforms_receive.length === 0 || transforms_receive.getItem(0).type !== SVGTransform.SVG_TRANSFORM_TRANSLATE) {
    var translate_receive = svg.createSVGTransform();
    translate_receive.setTranslate(0, 0);
    selectedElement_receive.transform.baseVal.insertItemBefore(translate_receive, 0);
  }
  transform_receive = transforms_receive.getItem(0);
};

const render_end_drag = (message) => {
  console.log("@receive end drag");
  if (selectedElement_receive) {
    transform_receive.setTranslate(message.x, message.y);
    selectedElement_receive = null;
  }
};

const render_erase = (message) => {
  let paths = svg.children;
  let selectedElement_receive;
  for (let i = 0; i < paths.length; i++) {
    if (paths[i].getAttribute("scribble_id") == message.scribble_id && paths[i].getAttribute("scribble_user") == message.scribble_user) {
      selectedElement_receive = paths[i];
      break;
    }
  }
  svg.removeChild(selectedElement_receive);
};

const render_clear_all = (message) => {
  console.log("@receive clear all");
  while (svg.firstChild) {
    svg.removeChild(svg.firstChild);
  }
};

const render_openPDF = (message, render_from_reconnection) => {
  console.log("@receive openPDF");
  // reset PDF SVGs
  $("#svgHolder > svg[id!=sample]").remove();
  if (render_from_reconnection) {
    __CURRENT_PAGE = 1;
    showPage(__CURRENT_PAGE, true);
  } else {
    let blobAsFile = dataURItoBlob(message.file, null);
    let blobFile = blobToFile(blobAsFile, message.filename);
    showPDF(URL.createObjectURL(blobFile));
  }
};

const render_prev_page_PDF = (message, render_from_reconnection) => {
  console.log("@receive prev_page_PDF");
  showPage(--__CURRENT_PAGE, render_from_reconnection);
};

const render_next_page_PDF = (message, render_from_reconnection) => {
  console.log("@receive next_page_PDF");
  showPage(++__CURRENT_PAGE, render_from_reconnection);

  if (__CURRENT_PAGE == __TOTAL_PAGES) {
    $("#pdf-next").attr("disabled", "true");
  }
};

const render_checkboxPDF = () => {
  console.log("@receive checkboxPDF");
  $("#svgHolder").attr("style", "width:100%; height:100%; position:absolute; display: none;");
  $("#pdf-container").attr("style", "text-align:center; width:100%; height:100%; position:absolute; pointer-events:none;");
  // $("button[id=PDF]").prop("style", "background-color: red;");
  rendered_view_mode = "PDF";
};

const render_checkboxPDFCanvas = () => {
  console.log("@receive checkboxPDFCanvas");
  $("#svgHolder").attr("style", "width:100%; height:100%; position:absolute;");
  $("#pdf-container").attr("style", "text-align:center; width:100%; height:100%; position:absolute; pointer-events:none;");
  // $("button[id=PDFCanvas]").prop("style", "background: red;");
  $("svg[id=" + __CURRENT_PAGE + "]").show();
  $("#svgHolder > svg[id!=" + __CURRENT_PAGE + "]").hide();
  svg = $("svg[id=" + __CURRENT_PAGE + "]")[0];
  rendered_view_mode = "PDFCanvas";
};

const render_checkboxCanvas = () => {
  console.log("@receive checkboxCanvas");
  $("#sample").attr("style", "width:100%; height:100%; position:absolute;");
  $("#pdf-container").attr("style", "text-align:center; width:100%; height:100%; position:absolute; pointer-events:none; display:none;");
  // $("button[id=Canvas]").prop("style", "background: red;");
  $("#svgHolder > svg[id!=sample]").hide();
  $("#sample").show();
  svg = $("#sample")[0];
  rendered_view_mode = "Canvas";
};

const getCanvasMousePos = (canvas, e) => {
  var rect = canvas.getBoundingClientRect();
  scaleX = canvas.width / rect.width;
  scaleY = canvas.height / rect.height;
  return {
    x: (e.clientX - rect.left) * scaleX,
    y: (e.clientY - rect.top) * scaleY
  };
};

// This function is used when the user draws after the mouse is being clicked.
var putPoint = function(e) {
  if (dragging) {
    let cavasMousePos = getCanvasMousePos(canvas, e);
    let x = cavasMousePos.x;
    let y = cavasMousePos.y + 10;

    context.lineTo(x, y);
    context.stroke();
    context.beginPath();
    context.arc(x, y, radius, 0, Math.PI*2);
    context.fill();
    context.beginPath();
    context.moveTo(x, y);
    motion = motion.concat(" L" + x + " " + y);

    sendMessage({
      "event": "putPoint",
      "x": x,
      "y": y,
      "radius": radius,
    });
  }
}

// This function is used when the user clicks the mouse in scribble mode.
var engage = function(e) {
  if (document.getElementById("btnScribble").disabled) {
    dragging = true;
    let cavasMousePos = getCanvasMousePos(canvas, e);
    let x = cavasMousePos.x;
    let y = cavasMousePos.y + 10;
    motion = "M"+ x + " " + y;
    btnClear.disabled = false;

    sendMessage({
      "event": "engage",
    });

    putPoint(e);
  }
}

// this function is triggered when the mouse is unclicked or the mouse leaves the SVG canvas
var disengage = function(e) {
  if (dragging) {
    let username = document.getElementsByClassName("username")[0].value;
    context.beginPath();
    dragging = false;

    // creating scribble
    let scribbleToCreateProperties = {
      "class": viewMode,
      "visibility": "visible",
      "scribble_id": scribble_id,
      "scribble_user": username,
      "path1": {
        "d": motion,
        "fill": "none",
        "visibility": "visible",
        "stroke": "white",
        "stroke_width": "20",
        "opacity": "0.0",
      },
      "path2": {
        "d": motion,
        "fill": "none",
        "visibility": "visible",
        "stroke": color,
        "stroke_width": size,
      },
    };
    createScribble(scribbleToCreateProperties);

    let pageUserInteraction = getPageUserInteraction();
    if (!pageUserInteraction) {
      pageUserInteraction = {
        "page": getCurrentPage(),
        "head": 0,
        "userInteraction": [],
      }
    }

    // append disengage event to user interactions
    let userInteractionObject = {
      "event": "disengage",
      "scribble_id": scribble_id,
      "scribble_user": username,
      "scribble": scribbleToCreateProperties,
    };

    let length = pageUserInteraction.userInteraction.length;
    pageUserInteraction.userInteraction.splice(pageUserInteraction.head, length-pageUserInteraction.head);
    pageUserInteraction.userInteraction.push(userInteractionObject);
    pageUserInteraction.head = pageUserInteraction.userInteraction.length;
    console.log("updated head: " + pageUserInteraction.head);
    updateUserInteraction(pageUserInteraction);

    sendMessage({
      "event": "disengage",
      "scribble": scribbleToCreateProperties,
      "d": motion,
      "fill": "none",
      "visibility": "visible",
      "stroke": color,
      "stroke-width": size,
      "scribble_id": scribble_id,
    });

    scribble_id++;
    motion = "";  // initalize svg motion
    context.clearRect(0, 0, canvas.width, canvas.height); // delete canvas drawing
  }
}

// this function undoes what the user previously made
var undo = function() {
  btnUndo.removeEventListener('click', undo);
  setTimeout(function () {
    btnUndo.addEventListener('click', undo);
  }, 800);

  let pageUserInteraction = getPageUserInteraction();
  if (!pageUserInteraction || pageUserInteraction.head == 0) {
    return;
  }
  let userInteractionObject = pageUserInteraction.userInteraction[--pageUserInteraction.head];
  let scribble_to_undo;
  let paths;

  switch (userInteractionObject.event) {
    case "disengage":
      scribble_to_undo = getScribble(userInteractionObject.scribble.scribble_id, userInteractionObject.scribble.scribble_user);
      svg.removeChild(scribble_to_undo);
      break;
    case "erase":
      createScribble(userInteractionObject.scribble);
      break;
    case "clear_all":
      let scribbles = userInteractionObject.scribbles;
      for (let i = 0; i < scribbles.length; i++) {
        createScribble(scribbles[i]);
      }
      break;
    default:
      return;
  }

  sendMessage({
    "event": "undo",
    "undo_event": userInteractionObject.event,
    "undo_scribble_id": userInteractionObject.scribble_id,
    "undo_scribble_user": userInteractionObject.scribble_user,
    "undo_scribble": userInteractionObject.scribble,
    "undo_scribbles": userInteractionObject.scribbles,
  });
}

var redo = function() {
  btnRedo.removeEventListener('click', redo);
  setTimeout(function () {
    btnRedo.addEventListener('click', redo);
  }, 800);

  let pageUserInteraction = getPageUserInteraction();
  if (!pageUserInteraction || pageUserInteraction.head == pageUserInteraction.userInteraction.length) {
    return;
  }
  let userInteractionObject = pageUserInteraction.userInteraction[pageUserInteraction.head++];
  let scribble_to_undo;
  let paths;

  switch (userInteractionObject.event) {
    case "disengage":
      createScribble(userInteractionObject.scribble);
      break;
    case "erase":
      scribble_to_redo = getScribble(userInteractionObject.scribble.scribble_id, userInteractionObject.scribble.scribble_user);
      svg.removeChild(scribble_to_redo);
      break;
    case "clear_all":
      while (svg.firstChild) {
        svg.removeChild(svg.firstChild);
      }
      break;
    default:
      return;
  }

  sendMessage({
    "event": "redo",
    "redo_event": userInteractionObject.event,
    "redo_scribble_id": userInteractionObject.scribble_id,
    "redo_scribble_user": userInteractionObject.scribble_user,
    "redo_scribble": userInteractionObject.scribble,
  });
}

// this function clears all the contents of the SVG element
var clearAll = function() {
  // clear all scribbles
  let scribblesToClear = [];
  while (svg.firstChild) {
    let scribbleToClearProperties = getScribbleProperties(svg.firstElementChild);
    scribblesToClear.push(scribbleToClearProperties);
    svg.removeChild(svg.firstChild);
  }

  sendMessage({
    "event": "clear_all",
  });

  let pageUserInteraction = getPageUserInteraction();
    if (!pageUserInteraction) {
      pageUserInteraction = {
        "page": getCurrentPage(),
        "head": 0,
        "userInteraction": [],
      }
    }

  // append clear_all event to user interactions
  let userInteractionObject = {
    "event": "clear_all",
    "scribbles": scribblesToClear,
  };

  let length = pageUserInteraction.userInteraction.length;
  pageUserInteraction.userInteraction.splice(pageUserInteraction.head, length-pageUserInteraction.head);
  pageUserInteraction.userInteraction.push(userInteractionObject);
  pageUserInteraction.head = pageUserInteraction.userInteraction.length;
  console.log("updated head: " + pageUserInteraction.head);
  updateUserInteraction(pageUserInteraction);
}

// this function will set the mode to move
var moveElement = function(){
  btnMove.disabled = true;
  btnMove.setAttribute("style", "border: none; background: none");
  btnScribble.disabled = false;
  btnScribble.removeAttribute("style");
  btnErase.disabled = false;
  btnErase.removeAttribute("style");
  svg.removeAttribute("class");
  $(svg).children().attr("class", "draggable");
  $(".btnGrpMove").removeAttr("style");
  viewMode = "draggable";
}

// this function will set the mode to scribble
var scribbleElement = function(){
  btnScribble.disabled = true;
  btnScribble.setAttribute("style", "border: none; background: none");
  btnMove.disabled = false;
  btnMove.removeAttribute("style");
  btnErase.disabled = false;
  btnErase.removeAttribute("style");
  $(svg).children().attr("class", "scribble");
  $(svg).attr("class", "scribble");
  viewMode = "scribble";
}

// this function will set the mode to erase
var eraseElement = function(){
  btnErase.disabled = true;
  btnErase.setAttribute("style", "border: none; background: none");
  btnMove.disabled = false;
  btnMove.removeAttribute("style");
  btnScribble.disabled = false;
  btnScribble.removeAttribute("style");
  var groups = svg.children;
  for (var i = 0; i < groups.length; i++) {
    groups[i].setAttribute("class", "erase");
  }
  viewMode = "erase";
}

// this transfers data from the pre-defined objects to the SVG canvas
var drag2 = function(ev){
  ev.dataTransfer.setData("text", ev.target.id);
  selectedElement = [];
}

// this function makes dropping allowable
var allowDrop = function(ev){
  ev.preventDefault();
}

// this function interprets what is being dropped and then renders it to the SVG canvas
var drop2 = function(ev) {
  var toCopy = ev.dataTransfer.getData("text");
  // btnUndo.disabled = false;
  var newG = document.createElementNS('http://www.w3.org/2000/svg', "g");
  newG.setAttribute("class", "draggable");
  newG.setAttribute("visibility", "visible");
  // newG.setAttribute("id", g_id++); change later
  if (toCopy.includes("vince")) {
    var paths = [];
    var newpath = document.createElementNS('http://www.w3.org/2000/svg', "path");
    newpath.setAttribute("d", "M9 17 L9 17 L11 18 L11 20 L11 26 L13 32 L14 36 L16 41 L17 44 L17 46 L18 47 L18 49 L21 50 L22 48 L24 43 L27 36 L29 30 L32 24 L33 21 L34 19 L36 16 L37 14");
    paths.push(newpath);
    newpath = document.createElementNS('http://www.w3.org/2000/svg', "path");
    newpath.setAttribute("d", "M40 42 L40 42 L41 43 L41 44 L41 45 L41 46 L41 48 L41 49 L41 50 L41 50 L41 52 L41 52");
    paths.push(newpath);
    newpath = document.createElementNS('http://www.w3.org/2000/svg', "path");
    newpath.setAttribute("d", "M48 28 L48 28 L47 27 L46 26 L45 26 L45 26 L44 25 L42 25 L42 25");
    paths.push(newpath);
    newpath = document.createElementNS('http://www.w3.org/2000/svg', "path");
    newpath.setAttribute("d", "M57 48 L57 48 L57 49 L57 50 L57 52 L57 56 L57 57 L57 59 L57 56 L58 55 L58 52 L59 51 L59 50 L60 49 L61 48 L63 48 L66 48 L67 48 L68 48 L70 48 L71 48 L72 48 L73 51 L73 53 L73 56 L74 58 L74 59 L75 60 L76 61 L77 61 L78 62 L79 63");
    paths.push(newpath);
    newpath = document.createElementNS('http://www.w3.org/2000/svg', "path");
    newpath.setAttribute("d", "M99 49 L99 49 L98 49 L97 49 L97 49 L96 49 L95 49 L95 51 L94 53 L94 57 L94 60 L94 62 L94 63 L94 65 L95 66 L96 67 L96 67 L99 67 L103 66 L105 65 L106 64 L110 63 L111 63 L115 62 L116 61 L117 60 L118 59 L119 58");
    paths.push(newpath);
    newpath = document.createElementNS('http://www.w3.org/2000/svg', "path");
    newpath.setAttribute("d", "M128 58 L128 58 L130 58 L131 58 L133 58 L136 58 L139 58 L141 58 L142 58 L144 58 L145 57 L146 56 L146 55 L146 52 L146 51 L145 50 L144 49 L143 49 L142 48 L141 48 L140 48 L139 48 L139 48 L137 48 L136 48 L134 48 L134 49 L133 50 L132 52 L131 54 L129 58 L128 61 L128 62 L128 63 L128 66 L128 68 L128 71 L128 73 L128 76 L128 77 L128 78 L129 79 L130 81 L131 81 L134 81 L136 81 L138 81 L140 81 L141 81 L145 80 L146 79 L148 78 L150 78 L152 77 L153 76 L154 75");
    paths.push(newpath);
    for (var i = 0; i < paths.length; i++) {
      newpath = paths[i];
      newpath.setAttribute("fill", "none");
      newpath.setAttribute("visibility", "visible");
      newpath.setAttribute("stroke", color);
      newpath.setAttribute("stroke-width", size);
      newG.appendChild(newpath);
    }
    ev.target.appendChild(newG);
  }
  // userInteraction.push(g_id - 1); change later
  var offset = getMousePosition(ev);
  var transforms = newG.transform.baseVal;
  var translate = svg.createSVGTransform();
  translate.setTranslate(0, 0);
  newG.transform.baseVal.insertItemBefore(translate, 0);
  transform = transforms.getItem(0);
  transform.setTranslate(offset.x, offset.y);
}

// this gets the mouse position relative to the SVG canvas
function getMousePosition(evt) {
    var CTM = svg.getScreenCTM();
    return {
      x: (evt.clientX - CTM.e) / CTM.a,
      y: (evt.clientY - CTM.f) / CTM.d
    };
}

// this implements the collapsibility of the color and size divs
var coll = document.getElementsByClassName("collapsible");
for (var i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.parentElement.getElementsByClassName("content");
        for (var i = 0; i < content.length; i++) {
          if (content[i].style.display === "inline") {
              content[i].style.display = "none";
          } else {
              content[i].style.display = "inline";
          }
      }
    });
}

// this implements the events when the color or size is set
var cont = document.getElementsByClassName("content");
for (var j = 0; j < cont.length; j++) {
    cont[j].addEventListener("click", function() {
    if (this.parentElement.classList.contains('divColor')) {
        color = this.getAttribute("color");
      } else {
        size = this.getAttribute("width");
      }
    });
}

function startDrag(evt) { // when the mouse is clicked
  let username = document.getElementsByClassName("username")[0].value;

  if (evt.target.parentElement.classList.contains('draggable') && document.getElementById("btnMove").disabled && evt.target.parentElement.getAttribute("scribble_user") == username) { // when in move mode
    selectedElement = evt.target.parentElement;
    offset = getMousePosition(evt);
    // Get all the transforms currently on this element
    var transforms = selectedElement.transform.baseVal;
    // Ensure the first transform is a translate transform
    if (transforms.length === 0 || transforms.getItem(0).type !== SVGTransform.SVG_TRANSFORM_TRANSLATE) {
      // Create an transform that translates by (0, 0)
      var translate = svg.createSVGTransform();
      translate.setTranslate(0, 0);
      // Add the translation to the front of the transforms list
      selectedElement.transform.baseVal.insertItemBefore(translate, 0);
    }
    // Get initial translation amount
    transform = transforms.getItem(0);
    offset.x -= transform.matrix.e;
    offset.y -= transform.matrix.f;

    sendMessage({
      "event": "start_drag",
      "scribble_id": selectedElement.getAttribute("scribble_id"),
      "scribble_user": selectedElement.getAttribute("scribble_user"),
    });
  } else if (document.getElementById("btnErase").disabled && evt.target.parentElement.classList.contains('erase') && evt.target.parentElement.getAttribute("scribble_user") == username) { // when in erase mode
    btnClear.disabled = false;

    // erase the target svg
    let scribbleToErase = evt.target.parentElement;
    let scribbleToEraseProperties = {
      "class": scribbleToErase.getAttribute("class"),
      "visibility": scribbleToErase.getAttribute("visibility"),
      "scribble_id": scribbleToErase.getAttribute("scribble_id"),
      "scribble_user": scribbleToErase.getAttribute("scribble_user"),
      "path1": {
        "d": scribbleToErase.children[0].getAttribute("d"),
        "fill": scribbleToErase.children[0].getAttribute("fill"),
        "visibility": scribbleToErase.children[0].getAttribute("visibility"),
        "stroke": scribbleToErase.children[0].getAttribute("stroke"),
        "stroke_width": scribbleToErase.children[0].getAttribute("stroke-width"),
        "opacity": scribbleToErase.children[0].getAttribute("opacity"),
      },
      "path2": {
        "d": scribbleToErase.children[1].getAttribute("d"),
        "fill": scribbleToErase.children[1].getAttribute("fill"),
        "visibility": scribbleToErase.children[1].getAttribute("visibility"),
        "stroke": scribbleToErase.children[1].getAttribute("stroke"),
        "stroke_width": scribbleToErase.children[1].getAttribute("stroke-width"),
      },
    };
    svg.removeChild(scribbleToErase);

    sendMessage({
      "event": "erase",
      "scribble_id": evt.target.parentElement.getAttribute("scribble_id"),
      "scribble_user": evt.target.parentElement.getAttribute("scribble_user"),
    });

    let pageUserInteraction = getPageUserInteraction();
    if (!pageUserInteraction) {
      pageUserInteraction = {
        "page": getCurrentPage(),
        "head": 0,
        "userInteraction": [],
      }
    }

    // append erase event to user interactions
    let userInteractionObject = {
      "event": "erase",
      "scribble_id": evt.target.parentElement.getAttribute("scribble_id"),
      "scribble_user": evt.target.parentElement.getAttribute("scribble_user"),
      "scribble": scribbleToEraseProperties,
    };

    let length = pageUserInteraction.userInteraction.length;
    pageUserInteraction.userInteraction.splice(pageUserInteraction.head, length-pageUserInteraction.head);
    pageUserInteraction.userInteraction.push(userInteractionObject);
    pageUserInteraction.head = pageUserInteraction.userInteraction.length;
    console.log("updated head: " + pageUserInteraction.head);
    updateUserInteraction(pageUserInteraction);
  }
}

function drag(evt) {
  if (selectedElement) {
    console.log("Should move.");
    var coord = getMousePosition(evt);
    transform.setTranslate(coord.x - offset.x, coord.y - offset.y);
  }
}

function endDrag(evt) { // when the mouse is clicked
  var coord = getMousePosition(evt);
  if (selectedElement) {
    sendMessage({
      "event": "end_drag",
      "x": coord.x - offset.x,
      "y": coord.y - offset.y,
    });
  }

  selectedElement = null;
  console.log("Ended");
}

fitPDFToContainer(pdf_canvas);
fitToContainer(cursor_canvas);
fitToContainer(canvas);

function fitToContainer(canvas) {
  canvas.style.width='100%';
  canvas.style.height='100%';
  canvas.width  = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
}

function fitPDFToContainer(canvas) {
  canvas.style.height='100%';
  canvas.width  = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
}

var __PDF_DOC,
  __TOTAL_PAGES,
  __PAGE_RENDERING_IN_PROGRESS = 0,
  __CANVAS = $('#pdf-canvas').get(0),
  __CANVAS_CTX = __CANVAS.getContext('2d');

var __CURRENT_PAGE = 1;

function showPDF(pdf_url, render_from_reconnection, check_view_mode) {
  $("#pdf-loader").show();

  PDFJS.getDocument({ url: pdf_url }).then(function(pdf_doc) {
    __PDF_DOC = pdf_doc;
    console.log("PDF_DOC: " + pdf_doc);
    __TOTAL_PAGES = __PDF_DOC.numPages;
    console.log(__TOTAL_PAGES + " is total");
    // Hide the pdf loader and show pdf container in HTML
    $("#pdf-loader").hide();
    $("#pdf-contents").show();
    $("#pdf-total-pages").text(__TOTAL_PAGES);


    if (render_from_reconnection) {
      showPage(__CURRENT_PAGE, false, check_view_mode);
    } else {
      showPage(1); // Show the first page
    }

  }).catch(function(error) {
    // If error re-show the upload button
    $("#pdf-loader").hide();
    $("#upload-button").show();

    alert(error.message);
  });;
}

function showPage(page_no, render_from_reconnection, check_view_mode) {
  __PAGE_RENDERING_IN_PROGRESS = 1;
  __CURRENT_PAGE = page_no;

  if (!render_from_reconnection) {
    // Disable Prev & Next buttons while page is being loaded
    $("#pdf-next, #pdf-prev").attr('disabled', 'disabled');

    // While page is being rendered hide the canvas and show a loading message
    $("#pdf-canvas").hide();
    $("#page-loader").show();

    // Update current page in HTML
    $("#pdf-current-page").text(page_no);

    // Fetch the page
    __PDF_DOC.getPage(page_no).then(function(page) {
      // As the canvas is of a fixed width we need to set the scale of the viewport accordingly
      var scale_required = __CANVAS.height / page.getViewport(1).height;

      // Get viewport of the page at required scale
      var viewport = page.getViewport(scale_required);

      // Set canvas height
      __CANVAS.width = viewport.width;

      var renderContext = {
        canvasContext: __CANVAS_CTX,
        viewport: viewport
      };

      // Render the page contents in the canvas
      page.render(renderContext).then(function() {
        __PAGE_RENDERING_IN_PROGRESS = 0;

        // Re-enable Prev & Next buttons
        if (presentor_id == user_id) {
          $("#pdf-next, #pdf-prev").removeAttr('disabled');
        }

        if (__CURRENT_PAGE == __TOTAL_PAGES) {
          $("#pdf-next").attr("disabled", "true");
        }
        if (__CURRENT_PAGE == 1) {
          $("#pdf-prev").attr("disabled", "true");
        }

        // if (rendered_view_mode) {
        //   if (rendered_view_mode == 'PDF' || rendered_view_mode == 'PDFCanvas') {
        //     // enable prev and next buttons
        //     $("#pdf-next").removeAttr("disabled");
        //     $("#pdf-prev").removeAttr("disabled");
        //   } else {
        //     // disable prev and next buttons
        //     $("#pdf-next").attr("disabled", "disabled");
        //     $("#pdf-prev").attr("disabled", "disabled");
        //   }
        // }

        // Show the canvas and hide the page loader
        $("#pdf-canvas").show();
        $("#page-loader").hide();
      });
    });
  }

  if (check_view_mode) {
    if (svg == $("#sample")[0]) {
      return;
    }
  }

  $("#svgHolder > svg[id!=" + __CURRENT_PAGE + "]").hide();
  if ($("svg[id=" + __CURRENT_PAGE + "]").length == 0) {
    var newSVG = document.createElementNS('http://www.w3.org/2000/svg',"svg");
    var classname = "svg";
    if (btnScribble.disabled) {
      classname = "svg scribble";
    }
    $(newSVG).attr({
      "id" : __CURRENT_PAGE,
      "class" : classname,
      "style" : "width:100%; height:100%; position:absolute; display:none;",
      "ondragover" : "allowDrop(event)",
      "ondrop" : "drop2(event)"
    });
    $("#svgHolder").append(newSVG);
  }
  $("svg[id=" + __CURRENT_PAGE + "]").show();
  svg = $("svg[id=" + __CURRENT_PAGE + "]")[0];
  if (user_id == presentor_id) {
    svg.addEventListener('mousedown', engage);
    svg.addEventListener('mousedown', startDrag);
    svg.addEventListener('mousemove', putPoint);
    svg.addEventListener('mousemove', drag);
    svg.addEventListener('mouseup', disengage);
    svg.addEventListener('mouseleave', disengage);
    svg.addEventListener('mouseup', endDrag);
    svg.addEventListener('mousemove', cursorMove);
  }
}

// document.getElementById("canvas-holder").on("scroll", f);
var xMousePos = 0;
var yMousePos = 0;
var lastScrolledLeft = 0;
var lastScrolledTop = 0;

$(document).mousemove(function(event) {
    captureMousePosition(event);
});

$("#canvas-holder").mousemove(function(event) {
  console.log("hihi");
    captureMousePosition(event);
});

    $("#pdf-container").scroll(function(event) {
        // if(lastScrolledLeft != $(document).scrollLeft()){
          console.log("Ent x");
            xMousePos -= lastScrolledLeft;
            lastScrolledLeft = $(document).scrollLeft();
            xMousePos += lastScrolledLeft;
        // }
        if(lastScrolledTop != $(document).scrollTop()){
          console.log("Ent y");
            yMousePos -= lastScrolledTop;
            lastScrolledTop = $(document).scrollTop();
            yMousePos += lastScrolledTop;
        }
        window.status = "x = " + xMousePos + " y = " + yMousePos;
        console.log("Kruu" + window.status);
    });
    $("#pdf-container").scroll(function(event) {
        // if(lastScrolledLeft != $(document).scrollLeft()){
          console.log("Ent x");
            xMousePos -= lastScrolledLeft;
            lastScrolledLeft = $(document).scrollLeft();
            xMousePos += lastScrolledLeft;
        // }
        if(lastScrolledTop != $(document).scrollTop()){
          console.log("Ent y");
            yMousePos -= lastScrolledTop;
            lastScrolledTop = $(document).scrollTop();
            yMousePos += lastScrolledTop;
        }
        window.status = "x = " + xMousePos + " y = " + yMousePos;
        console.log("Kruu" + window.status);
    });

function captureMousePosition(event){
    xMousePos = event.pageX;
    yMousePos = event.pageY;
    window.status = "x = " + xMousePos + " y = " + yMousePos;
        // console.log(window.status);
}

var cursorMove = function (e) {
  let canvasMousePos = getCanvasMousePos(canvas, e);
  cursorPos = canvasMousePos;
};

setInterval(function () {
  if (cursorPos) {
      console.log(`x: ${cursorPos.x}, y: ${cursorPos.y}`);
      let roomId = document.getElementsByClassName("roomId")[0].value;
      socket.send(JSON.stringify({
        "command": "send_cursor",
        "room": roomId,
        "message": {
          "event": "send_cursor",
          "x": cursorPos.x,
          "y": cursorPos.y,
        }
      }));
  }
}, 200);
