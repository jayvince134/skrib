import json
from channels import Channel
from channels.asgi import get_channel_layer
from channels.auth import channel_session_user_from_http, channel_session_user
from django.contrib.auth.models import User
from rooms.models import Room, Comment
from .settings import MSG_TYPE_LEAVE, MSG_TYPE_ENTER, NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS

@channel_session_user_from_http
@channel_session_user
def ws_connect(message):
    message.reply_channel.send({"accept": True})
    message.channel_session['comments'] = []
    message.reply_channel.send({
        "text": json.dumps({
            "connect": message.user.username,
        }),
    })


def ws_receive(message):
    payload = json.loads(message['text'])
    payload['reply_channel'] = message.content['reply_channel']
    Channel("comment.receive").send(payload)


@channel_session_user
def ws_disconnect(message):
    for room_id in message.channel_session.get("comments", set()):
        try:
            room = Room.objects.get(pk=room_id)
            room.websocket_group("comment").discard(message.reply_channel)
            # room.send_message(room.id, None, message.user, 'comment', MSG_TYPE_LEAVE)
        except Room.DoesNotExist:
            pass


@channel_session_user
def comment_join(message):
    room = Room.objects.get(pk=message["room"])
    # room.send_message(room.id, None, message.user, 'comment', MSG_TYPE_ENTER)
    room.websocket_group("comment").add(message.reply_channel)
    message.channel_session['comments'] = list(set(message.channel_session['comments']).union([room.id]))
    previous_comments = room.get_comments(message["client_cursor"])

    message.reply_channel.send({
        "text": json.dumps({
            "join": str(room.id),
            "title": room.title,
            "previous_comments": previous_comments,
        }),
    })


@channel_session_user
def comment_send(message):
    room = Room.objects.get(pk=message["room"])
    # room.send_message(room.id, message["message"], message.user, 'comment')
    room.send_comment(message["message"], message.user)
