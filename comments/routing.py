from channels import route
from .consumers import ws_connect, ws_receive, ws_disconnect, comment_join, comment_send

websocket_routing = [
    route("websocket.connect", ws_connect),
    route("websocket.receive", ws_receive),
    route("websocket.disconnect", ws_disconnect)
]

custom_routing = [
    route("comment.receive", comment_join, command="^join$"),
    route("comment.receive", comment_send, command="^send$"),
]
