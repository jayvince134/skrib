from django.conf import settings

NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS = getattr(settings, 'NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS', True)

MSG_TYPE_MESSAGE = 0
MSG_TYPE_ENTER = 1
MSG_TYPE_LEAVE = 2

MESSAGE_TYPES_CHOICES = getattr(settings, 'MESSAGE_TYPES_CHOICES', (
    (MSG_TYPE_MESSAGE, 'MESSAGE'),
    (MSG_TYPE_ENTER, 'ENTER'),
    (MSG_TYPE_LEAVE, 'LEAVE')
))

MESSAGE_TYPES_LIST = getattr(settings, 'MESSAGE_TYPES_LIST',
    [MSG_TYPE_MESSAGE,
     MSG_TYPE_ENTER,
     MSG_TYPE_LEAVE]
)
