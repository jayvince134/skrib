from django.urls import path
from . import views

app_name = 'accounts'
urlpatterns = [
    path('signin/', views.SignInView.as_view(), name='signin'),
	path('logout/', views.LogoutView.as_view(), name='logout'),
	path('', views.SignUpView.as_view(), name='signup'),
]
