from django import forms
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError # [Fixing Merge Conflicts] added this


class SignUpForm(forms.ModelForm):
	username = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder' : 'Username'}))
	password_submit = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder' : 'Password'}), min_length=8)
	password_confirm = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder' : 'Repeat Password'}), min_length=8)
	email = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder' : 'Email'}))

	class Meta:
		model = User
		fields = ["username", "password_submit", "password_confirm", "email"] # these are the fields in the form, may or may not use

	def clean_password_confirm(self):
		password_confirm = self.cleaned_data.get('password_confirm')
		password_submit = self.cleaned_data.get('password_submit')
		if password_confirm != password_submit:
			raise forms.ValidationError('Password does not match.')
		return password_confirm

	def clean_email(self):
		try:
			email = self.cleaned_data.get('email') # [Fixing Merge Conflicts] from cleaed_data
			validate_email(email)
			return email
		except ValidationError: # [Fixing Merge Conflicts] from validate_email.ValidationError
			raise forms.ValidationError('Use a Proper Email')

class SignInForm(forms.ModelForm):
	username = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder' : 'Username'}))
	password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder' : 'Password'}))

	class Meta:
		model = User
		fields = ["username", "password"] # these are the fields in the form
