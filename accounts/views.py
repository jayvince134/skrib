from django.shortcuts import render, redirect
from django.views.generic import View
from .forms import SignUpForm, SignInForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.
class SignUpView(View):
    form_class = SignUpForm
    template_name = 'accounts/signup.html'

    def get(self, request):
        user = self.request.user
        
        if user.is_authenticated and not user.profile.guest_user:
            return redirect('rooms:home')
        form = self.form_class(None)
        return render(request, self.template_name, {'form' : form})

    def post(self, request):
        form = self.form_class(request.POST)
        if (form.is_valid()):
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password_submit = form.cleaned_data['password_submit']
            password_confirm = form.cleaned_data['password_confirm']
            user.set_password(password_submit)
            user.save()
            user = authenticate(username=username, password=password_submit)
            if user is not None:
                login(request, user)
                return redirect('rooms:home')
        return render(request, self.template_name, {'form' : form})

class SignInView(View):
    form_class = SignInForm
    template_name = 'accounts/signin.html'

    def get(self, request):
        user = self.request.user

        if user.is_authenticated and not user.profile.guest_user:
            return redirect('rooms:home')
        form = self.form_class(None)
        return render(request, self.template_name, {'form' : form})

    def post(self, request):
        form = self.form_class(request.POST)

        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('rooms:home')

        return render(request, self.template_name, {'form' : form})

class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('accounts:signup')
