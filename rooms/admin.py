from django.contrib import admin
from .models import Room,Comment
from rooms.forms import PrivateRoomAdmin
import tagulous.admin

class RoomAdmin(admin.ModelAdmin):
    list_display = ('presentor','title','date_created','is_private')
    form = PrivateRoomAdmin
    fieldsets = (
        (None, {
            'fields': ('presentor','title', 'description', 'pdf_file', 'pdf_file_name'),
        }),
        ('Category', {
            'fields': ('room_categories', 'is_private',),
            'classes': ('is_room_private',)
        }),
        (None, {
            'fields': (('users','guest_users'),),
            'classes': ('send_email',)
        }),
        ('Server', {
            'fields': ('server_cursor_comments', 'comments', 'server_cursor_scribbles', 'scribbles',)
        }),
    )

    class Media:
        js = ('rooms/js/private-room.js',)

tagulous.admin.register(Room, RoomAdmin)

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass
