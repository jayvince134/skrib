from channels import route
from .consumers import ws_connect, ws_receive, ws_disconnect, scribble_join, scribble_send, scribble_send_cursor

websocket_routing = [
    route("websocket.connect", ws_connect),
    route("websocket.receive", ws_receive),
    route("websocket.disconnect", ws_disconnect)
]

custom_routing = [
    route("scribble.receive", scribble_join, command="^join$"),
    route("scribble.receive", scribble_send, command="^send$"),
    route("scribble.receive", scribble_send_cursor, command="^send_cursor$")
]
