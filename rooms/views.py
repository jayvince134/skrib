from django.shortcuts import render, redirect, get_object_or_404
from django.views import generic
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from .models import Room
from .forms import RoomForm
from .streaming import CreatingWebStreaming
from accounts.models import Profile
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.contrib.auth import login
import json

def sending_invites_to_other_users(presenter, room_key, room_title, invited_users, request):
    presenter = User.objects.get(username=presenter)
    mail_subject = 'This is an invite to ' + room_title + ' for you to join in'
    mail_message = 'Please paste the link or enter ' + request.get_host() + reverse('rooms:join-room', args=(room_key,))
    send_mail(mail_subject, mail_message, presenter.email, invited_users, fail_silently=False,)

class RoomHomeView(generic.View):
    template_name = "rooms/classroom-lists.html"
    login_url = 'accounts:signin'
    redirect_field_name = 'redirect_to'
    form_class = RoomForm
    model = Room
    room_category_list = [room_category[1] for room_category in Room.ROOM_CATEGORIES]
    users = json.dumps([user[0] for user in User.objects.values_list('email')])

    def post(self, request):
        form = self.form_class(request.POST)
        if (form.is_valid()):
            room = form.save(commit=False)
            room_session = CreatingWebStreaming()
            session = room_session.create_session()
            room.room_streaming = session.session_id
            room_email_skrib_and_guest_users = request.POST.getlist('tags')
            room.presentor = User.objects.get(pk=self.request.user.id)
            room.save()
            if (room_email_skrib_and_guest_users):
                sending_invites_to_other_users(presenter=room.presentor, room_key=room.id, room_title=room.title, invited_users=room_email_skrib_and_guest_users, request=request)
            return HttpResponseRedirect(reverse('rooms:join-room', args=(room.id,)))

    def get(self, request):
        user = self.request.user
        rooms = Room.objects.filter(date_created__lte=timezone.now(), is_private=False).order_by('-date_created')
        newCategoryList = []
        for room in rooms:
            if room.room_categories not in newCategoryList:
                newCategoryList.append(room.room_categories)
        if user.is_authenticated:            
            if request.is_ajax():
                room_name = request.GET.get('searched_room_name')
                searched_classrooms = Room.objects.filter(title__contains=room_name)
                context = []
                for room in searched_classrooms:
                    data = {
                        'presentor' : room.presentor.username,
                        'title' : room.title,
                        'id' : room.id,
                        'description' : room.description
                    }
                    context.append(data)
                return JsonResponse({'searched_classrooms' : context})
            else:
                form = self.form_class(None)
                return render(request, self.template_name, {
                        'form' : form,
                        'created_classrooms' : rooms,
                        'room_category_list' : newCategoryList,
                        'users_list' : self.users
                    })
        else:
            return redirect('accounts:signup')

class JoinRoomView(generic.DetailView):
    model = Room
    template_name = "rooms/classroom-detail.html"

    def get(self, request, pk):
        self.object = self.get_object()
        if not request.user.is_authenticated:
            user = User(username='guest{}'.format(User.objects.count()))
            user.save()
            user.profile.guest_user = True
            login(request, user)
        return render(request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        room = Room.objects.get(pk=self.kwargs.get('pk'))
        room_session = CreatingWebStreaming()
        if not room.room_streaming and self.request.user.username != room.presentor:
            session = room_session.create_session()
            room.room_streaming = session.session_id
            room.save()
        token = room_session.generate_token(room.room_streaming)
        context = super().get_context_data(**kwargs)
        context['api_key'] = room_session.api_key
        context['sessionId'] = room.room_streaming
        context['token'] = token
        return context

class UpdateRoomDescription(generic.View):
    def post(self, request, *args, **kwargs):
        room_description = request.POST.get('room_description')
        room_id = request.POST.get('room_id')
        room = get_object_or_404(Room, id=room_id)
        room.description = room_description
        room.save()
        context = {
            'roomDescription' : room_description,
        }
        return JsonResponse(context)
