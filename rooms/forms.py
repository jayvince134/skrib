from django import forms
from django.contrib.auth.models import User
from django.core.validators import validate_email
from .models import Room

class RoomForm(forms.ModelForm):
    room_categories = forms.CharField(widget=forms.Select(choices=Room.ROOM_CATEGORIES))
    is_private = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={
            'class' : 'switch-input',
            'name' : 'is_private'
        }))

    class Meta:
        model = Room
        fields = ['title', 'room_categories', 'is_private']

    def __init__(self, *args, **kwargs):
        super(RoomForm, self).__init__(*args, **kwargs)
        self.fields['is_private'].required = False

class MultipleEmailField(forms.Field):
    def to_python(self, value):
        if not value:
            return []
        return value.split(', ')

    def validate(self, value):
        super().validate(value)
        for email in value:
            validate_email(email)

class PrivateRoomAdmin(forms.ModelForm):
    room_categories = forms.CharField(widget=forms.Select(choices=Room.ROOM_CATEGORIES))
    users = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
        choices=[(user.username, user.username) for user in User.objects.all()]
    )
    guest_users = MultipleEmailField()

    def __init__(self, *args, **kwargs):
        super(PrivateRoomAdmin, self).__init__(*args, **kwargs)
        self.fields['users'].required = False
        self.fields['guest_users'].required = False

    class Meta:
        model = Room
        fields = ['title', 'description', 'room_categories', 'is_private']
