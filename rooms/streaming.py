from opentok import OpenTok, MediaModes

class CreatingWebStreaming():
    api_key = '46150472'
    api_secret = 'bd94039bc3ab01dc435c5959c8ab5b71d9d76ce6'

    opentok = OpenTok(api_key, api_secret)

    def generate_token(self, session):
        token = self.opentok.generate_token(session)
        return token

    def create_session(self):
        session = self.opentok.create_session(media_mode=MediaModes.routed)
        return session
