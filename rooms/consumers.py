import json
from channels import Channel
from channels.asgi import get_channel_layer
from channels.auth import channel_session_user_from_http, channel_session_user
from django.contrib.auth.models import User
from .models import Room, Comment
from .settings import MSG_TYPE_LEAVE, MSG_TYPE_ENTER

@channel_session_user_from_http
@channel_session_user
def ws_connect(message):
    message.reply_channel.send({"accept": True})
    message.channel_session['scribble'] = []
    message.reply_channel.send({
        "text": json.dumps({
            "connect": message.user.username,
        }),
    })


def ws_receive(message):
    payload = json.loads(message['text'])
    payload['reply_channel'] = message.content['reply_channel']
    Channel("scribble.receive").send(payload)


@channel_session_user
def ws_disconnect(message):
    for room_id in message.channel_session.get("scribble", set()):
        try:
            room = Room.objects.get(pk=room_id)
            room.websocket_group("scribble").discard(message.reply_channel)
            # room.send_message(None, message.user, 'scribble', MSG_TYPE_LEAVE)
        except Room.DoesNotExist:
            pass


@channel_session_user
def scribble_join(message):
    room = Room.objects.get(pk=message["room"])
    # room.send_message(None, message.user, 'scribble', MSG_TYPE_ENTER)
    room.websocket_group("scribble").add(message.reply_channel)
    message.channel_session['scribble'] = list(set(message.channel_session['scribble']).union([room.id]))
    previous_scribbles = room.get_scribbles(message["client_cursor"])


    message.reply_channel.send({
        "text": json.dumps({
            "join": str(room.id),
            "title": room.title,
            "previous_scribbles": previous_scribbles,
            "pdf_file": room.pdf_file,
            "pdf_file_name": room.pdf_file_name,
        }),
    })


@channel_session_user
def scribble_send(message):
    room = Room.objects.get(pk=message["room"])
    room.send_message(room.id, message["message"], message.user, 'scribble')


@channel_session_user
def scribble_send_cursor(message):
    room = Room.objects.get(pk=message["room"])
    room.send_message_cursor(message["message"], message.user, 'scribble')
