import json
from django.db import models
from django.contrib.auth.models import User
from django.utils.six import python_2_unicode_compatible
from datetime import datetime
from channels import Group
from comments.settings import MSG_TYPE_MESSAGE
from django.urls import reverse
from django.db import transaction
from django.db.models import F

from django.contrib.auth.models import User
import tagulous.models

@python_2_unicode_compatible
class Room(models.Model):
    ROOM_CATEGORIES = [
        ('Educational', 'Educational'),
        ('Science', 'Science'),
        ('Music','Music'),
        ('Food', 'Food'),
        ('Socializing', 'Socializing'),
        ('Others', 'Others')
    ]

    presentor = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=200)
    description = models.TextField(default="No Room Description Available")
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    is_private = models.BooleanField(default=False)
    room_categories = models.CharField(choices=ROOM_CATEGORIES, max_length=50, default='Educational')
    room_streaming = models.CharField(max_length=300)
    pdf_file = models.TextField(null=True, blank=True)
    pdf_file_name = models.TextField(null=True, blank=True)

    def is_presentor(self, user):
        return self.presentor.username == user

    def get_room_category(self):
        return self.room_categories

    def is_room_private(self):
        return self.is_private == True

    def get_absolute_url(self):
        return reverse('rooms:join-room', kwargs={ 'pk': self.pk })

    server_cursor_comments = models.BigIntegerField(default=0)
    comments = models.TextField(default="", blank=True)
    server_cursor_scribbles = models.BigIntegerField(default=0)
    scribbles = models.TextField(default="", blank=True)

    def __str__(self):
        return self.title

    def websocket_group(self, source):
        return Group("room-%s-%s" % (self.id, source))


    @classmethod
    def send_message(cls, id, message, user, source, msg_type=MSG_TYPE_MESSAGE):
        with transaction.atomic():
            room = (
                cls.objects
                .select_for_update(nowait=False)
                .get(id=id)
            )
            final_msg = {"room": str(id), "message": message, "username": user.username, "msg_type": msg_type, "server_cursor": room.server_cursor_scribbles}
            if source == 'comment':
                # room.comments = str(room.comments) + str(room.server_cursor_comments) + " " +  json.dumps(final_msg) + "\n"
                # room.server_cursor_comments += 1
                pass
            elif source == 'scribble':
                save_msg = final_msg.copy()
                # save_msg["message"] = [message for message in save_msg["message"]
                #                         if message["event"] not in ["putPoint", "prev_page_PDF", "next_page_PDF",
                #                             "checkboxPDF", "checkboxPDFCanvas", "checkboxCanvas"]]
                save_msg_list = []
                for message in save_msg["message"]:
                    if message["event"] not in ["putPoint"]:
                        save_msg_list.append(message)
                    if message["event"] == "openPDF":
                        room.pdf_file = message["file"]
                        room.pdf_file_name = message["filename"]
                save_msg["message"] = save_msg_list
                room.scribbles = str(room.scribbles) + str(room.server_cursor_scribbles) + " " + json.dumps(save_msg) + "\n"
                room.server_cursor_scribbles += 1

            room.save()

            room.websocket_group(source).send(
                {"text": json.dumps(final_msg)}
            )
        return room

    def send_message_cursor(self, message, user, source):
        final_msg = {"room": str(self.id), "message": message, "username": user.username}
        self.websocket_group(source).send(
            {"text": json.dumps(final_msg)}
        )

    def send_comment(self, message, user, msg_type=MSG_TYPE_MESSAGE):
        with transaction.atomic():
            final_msg = {"room": self.id, "message": message, "username": user.username, "msg_type": msg_type}

            comment = Comment(comment_source_room=self, comment_room_cursor=self.server_cursor_comments, commenter=user, comment_text=message)
            comment.save()

            self.server_cursor_comments = F('server_cursor_comments') + 1
            self.save()

            self.websocket_group('comment').send(
                {"text": json.dumps(final_msg)}
            )

    def get_comments(self, client_cursor):
        comments = Comment.objects.filter(comment_source_room=self, comment_room_cursor__gte=client_cursor).order_by('comment_room_cursor')
        comments_list = [{"username":comment.commenter.username, "message":comment.comment_text, "msg_type":0} for comment in comments]
        return comments_list

    def get_scribbles(self, client_cursor):
        scribbles_list = str(self.scribbles).split('\n')[client_cursor:-1]
        scribbles_list = [(scribble[scribble.find(' ')+1:]) for scribble in scribbles_list]
        return scribbles_list

class Comment(models.Model):
    comment_source_room = models.ForeignKey(Room, on_delete=models.CASCADE)
    comment_room_cursor = models.IntegerField(default=0)
    commenter = models.ForeignKey(User, on_delete=models.CASCADE)
    comment_text = models.TextField()
    comment_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comment_text[:50]
