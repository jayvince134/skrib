$( function() {
    
    $("#post-room-description").on("submit", function (event) {
        event.preventDefault();
        var roomDescription = $('#room-description-content').text();

        var form = $(this).closest("form");
        $.ajax({
            method: 'POST',
            url: form.attr('data-url'),
            data: {
                'room_id' : room_id,
                'room_description' : roomDescription,
                'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val()
            },
            success: function(data, textStatus, jqXHR) {
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            },
            dataType: 'json'
        });
    });
});
