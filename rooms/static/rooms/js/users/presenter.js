(function () {
    var publisher;
    var session;
    var handleError = function (error) {
        if (error) {
            alert(error.message);
        }
    }

    var init = function () {
        var status = document.getElementById('status').value;
        if (status == 'active') {
            var publisherOptions = {
                videoSource: null
            };
            var publisherGetElementById = document.getElementById('publisher');

            session = OT.initSession(apiKey, sessionId);
            publisher = OT.initPublisher(publisherGetElementById, publisherOptions, handleError);
            session.connect(token, function (error) {
                if (error) {
                    handleError(error);
                } else {
                    session.publish(publisher, handleError);
                    document.getElementById('startStop').disabled = false;
                }
            });

        } else if (status == 'ended') {
            session.unpublish(publisher);
            session.disconnect();
        }
    }

    var updateStatus = function (broadCastStatus) {
        var startStopButton = document.getElementById('startStop');
        document.getElementById('status').value = broadCastStatus;
        init();

        if (broadCastStatus == 'active') {
            startStopButton.innerHTML = 'Disable Audio Feed';
        } else if (broadCastStatus == 'ended') {
            startStopButton.innerHTML = 'Enable Audio Feed';
            document.getElementById('status').value = 'waiting';
            insertNewPublisherDiv();
        }
    }

    var insertNewPublisherDiv = function () {
        var newPublisherDiv = document.createElement('div');
        var videosDiv = document.getElementsByClassName('videos')[0];
        var subscriberDiv = document.getElementById('subscriber');
        newPublisherDiv.setAttribute('id', 'publisher');
        videosDiv.insertBefore(newPublisherDiv, subscriberDiv);
    }

    var startAndEndBroadcasting = function () {
        var status = document.getElementById('status').value;

        if (status == 'waiting') {
            document.getElementById('startStop').disabled = true;
            updateStatus('active');
        } else if (status == 'active') {
            updateStatus('ended');
        }
    }
    document.getElementById('startStop').addEventListener('click', startAndEndBroadcasting);
    document.getElementById('status').value = 'waiting';
}());
