(function () {    
    var handleError = function (error) {
        if (error) {
            alert(error.message);
        }
    }

    var init = function () {        
        var session = OT.initSession(apiKey, sessionId);    
        var subscriberGetElementById = document.getElementById('subscriber');
        session.connect(token, function (error) {
            if (error) {
                handleError(error);
            } else {
                session.on(
                    'streamCreated', function (event) {
                        subsciberOptions = {
                            insertMode: 'append',
                            subscribeToAudio: true,
                        };
                        session.subscribe(event.stream, subscriberGetElementById, subsciberOptions, handleError);
                    }
                );
            }
        });
    }

    document.addEventListener('DOMContentLoaded', init);

}());
