$( function() {
    
    function split( val ) {
        return val.split( /,\s*/ );
    }

    function extractLast( term ) {
        return split( term ).pop();
    }
    
    $("#invite_guest_or_skrib_users").tagit({
        autocomplete: {
            source: function( request, response ) {
                response( $.ui.autocomplete.filter(
                    users_list, extractLast( request.term ) ) );           
             }
        },
        singleFieldDelimiter: ',',
        allowSpaces: false,
    });

    $("#search-room").bind('keypress', function (event) {
        if(event.which == 13) {
            event.preventDefault();
        }
    }).on("input", function (event) {
        if($(this).val() != '') {
            var form = $(this).closest("form")
            $.ajax({
                method: 'GET',
                url: form.attr("action"),
                data: { 
                    'searched_room_name' : $(this).val(),
                },
                success: checkIfSearchIsImplemented,
                error: function(error) {
                    console.log(error.message)
                },
                dataType: 'json'
            });
        } else {
            checkIfSearchIsImplemented()
        }
    });

    function checkIfSearchIsImplemented(data, textStatus, jqXHR) {
        if(data == undefined) {
            $('.search-results').css('display', 'none');
            $('.main-body').css('display', 'block');
            var passHtmlDataToClassroomList = "<p>No searched classroom found</p>"
            $('#searched-classroom-results').html(passHtmlDataToClassroomList)
        }
        else {
            $('.search-results').css('display', 'block')
            $('.main-body').css('display', 'none')
            var passHtmlDataToClassroomList = "<div class='main first-div'>";
            for(rooms in data.searched_classrooms) {            
                passHtmlDataToClassroomList += `
                <div class="card">
                    
                    <a href="/rooms/${data.searched_classrooms[rooms].id}/" class="navbtn color-main">
                        <img src='/static/global/img/logo-draft2-sqr.svg' class="img-container">
                        <div class="card-desc">
                            <p class="text">
                                ${data.searched_classrooms[rooms].description}
                            </p>
                        </div>
                        <div class="container">
                            <h3><span>${data.searched_classrooms[rooms].title}</span></h3>
                            <span class="profile-pic-small"></span><p>${data.searched_classrooms[rooms].presentor}</p>
                        </div>
                    </a>
                </div>`
            }
            passHtmlDataToClassroomList += "</div>";
            $('#searched-classroom-results').html(passHtmlDataToClassroomList)

        }
    
    }
});
