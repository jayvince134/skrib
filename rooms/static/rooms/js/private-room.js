(function($) {
    $(function() {
        var selectField = $('#id_room_type'),
            verified = $('.send_email');

        function toggleVerified(value) {
            if (value === 'Private') {
                verified.show();
            } else {
                verified.hide();
            }
        }

        // show/hide on load based on pervious value of selectField
        toggleVerified(selectField.val());

        // show/hide on change
        selectField.change(function() {
            toggleVerified($(this).val());
        });
    });
})(django.jQuery);