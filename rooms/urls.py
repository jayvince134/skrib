from django.urls import path
from . import views

app_name = 'rooms'
urlpatterns = [

	path('', views.RoomHomeView.as_view(), name='home'),
	path('<int:pk>/', views.JoinRoomView.as_view(), name='join-room'),

	# ajax views
	path('<int:pk>/update-room-description/', views.UpdateRoomDescription.as_view(), name='update-room-description'),
	path('search/', views.RoomHomeView.as_view(), name='room-search'), # add name='room-search'
]
