from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('accounts.urls', namespace='accounts')),
    path('admin/', admin.site.urls),
    path('rooms/', include('rooms.urls', namespace='rooms')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
