from channels import include


channel_routing = [
    include("comments.routing.websocket_routing", path=r"^/comments/stream"),
    include("comments.routing.custom_routing"),
    include("rooms.routing.websocket_routing", path=r"^/scribble/stream"),
    include("rooms.routing.custom_routing"),
]
